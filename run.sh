#!/usr/bin/env bash
set -euo pipefail

_run_dir="$( cd "$( dirname $0 )" && pwd )"

[ ! -f ${_run_dir}/docker.properties ] && echo "${_run_dir}/docker.properties" && exit 1
source ${_run_dir}/docker.properties

sudo docker run --rm -it -v ${_run_dir}/conf:/wpwatcher/.wpwatcher ${_docker_image}:${_docker_image_version} 
