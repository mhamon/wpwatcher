#!/usr/bin/env bash
set -uo pipefail

_run_dir="$( cd "$( dirname $0 )" && pwd )"
[ ! -f ${_run_dir}/docker.properties ] && echo "${_run_dir}/docker.properties" && exit 1
source ${_run_dir}/docker.properties

echo -e "Building ${_docker_image} image\n"
sudo docker build -t ${_docker_image}:${_docker_image_version} $_run_dir
