# WPWatcher

Ce repo se base sur [WPWatcher](https://github.com/tristanlatr/WPWatcher) l'execution est mise dans un conteneur et la configuration dans le dossier conf/
Le but est de notifier des vulnérabilité sur les sites wordpress

### Utilisation
#### Recuperer le repo WPWatcher
```bash
git submodule update --init --recursive
```
#### Build de l'image
```bash
./build.sh
```

#### Execution
```bash
./run.sh
```

#### Configuration
La notification est envoyée par défaut à
```bash
grep "email_to" conf/wpwatcher.conf
```

#### Notes
L'image docker est poussé sur la registry [gitlab](https://gitlab.com/mhamon/wpwatcher/container_registry)
```bash
docker pull registry.gitlab.com/mhamon/wpwatcher
```
